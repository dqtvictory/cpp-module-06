#include <iostream>
#include <iomanip>
#include <string>
#include <sstream>
#include <cstdlib>
#include <math.h>	// cmath


static bool	argValid(std::string arg)
{
	// Check whole string
	const std::string	validStr[8] = {
		"inf", "+inf", "-inf", "inff", "+inff", "-inff", "nan", "nanf"
	};
	for (int i = 0; i < 8; i++)
		if (arg == validStr[i])
			return (true);

	// Check first char
	int	c = *(arg.begin());
	if (!isdigit(c) && c != '-')
		return (false);
	
	// Check all chars afterward
	std::string::iterator it;
	bool	seenDot = false;
	bool	seenF	= false;

	for (it = arg.begin() + 1; it != arg.end(); it++)
	{
		c = *it;
		if (c == '.')
		{
			if (seenDot)
				return (false);
			seenDot = true;
		}
		else if (c == 'f')
		{
			if (seenF || !seenDot || !isdigit(*(it - 1)))
				return (false);
			seenF = true;
		}
		else if (!isdigit(c))
			return (false);
	}

	// Check last char
	if (c == '.' || (isdigit(c) && seenF))
		return (false);

	return (true);
}

static void	my_putchar(double num)
{
	if (isinf(num) || isnan(num))
		std::cout << "Impossible";
	else
	{
		int	c = static_cast<int>(num);
		if (' ' <= c && c <= '~')
			std::cout << '\'' << (char)c << '\'';
		else
			std::cout << "Non displayable";
	}
	std::cout << '\n';
}

static void	my_putint(double num)
{
	if (isinf(num) || isnan(num))
		std::cout << "Impossible";
	else
	{
		int	n = static_cast<int>(num);
		std::cout << n;
	}
	std::cout << '\n';
}

static void	my_putf(double num)
{
	std::stringstream	ss;
	ss << static_cast<float>(num);

	std::string	output = ss.str();

	if (!isinf(num) && !isnan(num))
	{
		size_t	dot = output.find('.');
		if (dot == std::string::npos)	// dot not found
			output += ".0";
	}
	
	std::cout << output << "f\n";
}

static void	my_putd(double num)
{
	std::stringstream	ss;
	ss << static_cast<float>(num);

	std::string	output = ss.str();

	if (!isinf(num) && !isnan(num))
	{
		size_t	dot = output.find('.');
		if (dot == std::string::npos)	// dot not found
			output += ".0";
	}
	
	std::cout << output << '\n';
}

int	main(int ac, char **av)
{
	if (ac != 2 || !argValid(av[1]))
	{
		std::cerr << "Invalid argument\n";
		return (1);
	}

	double num = atof(av[1]);

	std::cout << "char:   "; my_putchar(num);
	std::cout << "int:    "; my_putint(num);
	std::cout << "float:  "; my_putf(num);
	std::cout << "double: "; my_putd(num);
}