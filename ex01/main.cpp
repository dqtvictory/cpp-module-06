#include <ctime>
#include <cstdlib>
#include <stdint.h>
#include "Data.hpp"

#define CYAN	"\033[96m"
#define NOCOLOR	"\033[0m"

uintptr_t	serialize(Data *ptr)
{
	return (reinterpret_cast<uintptr_t>(ptr));
}

Data	*deserialize(uintptr_t raw)
{
	return (reinterpret_cast<Data *>(raw));
}

int	main(void)
{
	srand(time(NULL));

	const std::string	randomStr[] =
	{
		"Pony", "Unicorn", "French Bulldog",
		"Rainbow", "42 Paris", "Breaking Bad"
	};
	const size_t	size = sizeof(randomStr) / sizeof(std::string);

	// Generate data
	std::string	s = randomStr[rand() % size];
	int			i = rand();
	float		f = static_cast<float>(rand()) / static_cast<float>(rand());
	Data		*defaultData = new Data();
	Data		*randomData = new Data(s, i, f);

	// Display data in correct form
	std::cout	<< CYAN << "\nBEFORE SERIALIZATION\n\n" << NOCOLOR
				<< *defaultData << '\n' << *randomData << '\n';

	// Serialize data
	uintptr_t	seriDefData = serialize(defaultData);
	uintptr_t	seriRanData = serialize(randomData);

	// Display serialized data
	std::cout	<< CYAN << "\nAFTER SERIALIZATION\n\n" << NOCOLOR
				<< "Default data: " << seriDefData << '\n'
				<< "Random data:  " << seriRanData << "\n\n";

	// Deserialize data back to normal
	Data	*deseriDef = deserialize(seriDefData);
	Data	*deseriRan = deserialize(seriRanData);

	// Display deserialized data
	std::cout	<< CYAN << "\nAFTER DE-SERIALIZATION\n\n" << NOCOLOR
				<< *deseriDef << '\n' << *deseriRan << '\n';

	delete defaultData;
	delete randomData;
}