#include "Data.hpp"

Data::Data(void) : _s("Default string"), _i(0), _f(0.0f)
{}

Data::Data(const std::string &s, int i, float f) : _s(s), _i(i), _f(f)
{}

Data::Data(const Data &d) : _s(d._s), _i(d._i), _f(d._f)
{}

Data	&Data::operator=(const Data &d)
{
	return ((Data &)d);
}

Data::~Data(void)
{}

const std::string	&Data::getS(void) const
{
	return (_s);
}

int		Data::getI(void) const
{
	return (_i);
}

float	Data::getF(void) const
{
	return (_f);
}

std::ostream	&operator<<(std::ostream &os, const Data &d)
{
	os	<< "====== BEGIN DATA ======\n"
		<< "Str: " << d.getS() << '\n'
		<< "Int: " << d.getI() << '\n'
		<< "Flt: " << d.getF() << '\n'
		<< "======= END DATA =======\n";
	return (os);
}
