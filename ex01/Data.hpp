#ifndef DATA_HPP
#define DATA_HPP

#include <iostream>

class Data
{

private:
	const std::string	_s;
	const int			_i;
	const float			_f;

public:
	Data(void);
	Data(const std::string &s, int i, float f);
	Data(const Data &d);
	Data	&operator=(const Data &d);
	~Data(void);

	const std::string	&getS(void) const;
	int					getI(void) const;
	float				getF(void) const;

};

std::ostream	&operator<<(std::ostream &os, const Data &d);

#endif
