#include <iostream>
#include <cstdlib>
#include <ctime>
#include "Classes.hpp"

static Base	*generate(void)
{
	int	r = rand() % 3;

	if (r == 0)
	{
		std::cout << "\nA is created\n\n";
		return (new A());
	}
	if (r == 1)
	{
		std::cout << "\nB is created\n\n";
		return (new B());
	}
	std::cout << "\nC is created\n\n";
	return (new C());
}

static void	identify(Base *p)
{
	std::cout << "Identifying from pointer:\n";
	if (dynamic_cast<A *>(p))
		std::cout << "Type is A\n\n";
	else if (dynamic_cast<B *>(p))
		std::cout << "Type is B\n\n";
	else if (dynamic_cast<C *>(p))
		std::cout << "Type is C\n\n";
	else
		std::cout << "Unknown type\n\n";
}

static void identify(Base &p)
{
	std::cout << "Identifying from reference:\n";
	try
	{
		A	a = dynamic_cast<A &>(p);
		std::cout << "Type is A\n\n";
		return ;
	}
	catch (std::exception &e) {}

	try
	{
		B	b = dynamic_cast<B &>(p);
		std::cout << "Type is B\n\n";
		return ;
	}
	catch (std::exception &e) {}

	try
	{
		C	c = dynamic_cast<C &>(p);
		std::cout << "Type is C\n\n";
		return ;
	}
	catch (std::exception &e) {}

	std::cout << "Unknown type\n\n";
}

int	main(void)
{
	srand(time(NULL));
	Base	*p = generate();
	identify(p);	// from pointer
	identify(*p);	// from reference
	delete p;
}